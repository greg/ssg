FROM ubuntu:latest

RUN apt update && apt upgrade -y
RUN DEBIAN_FRONTEND=noninteractive apt install -y bats cmake make expat git libopenscap8 libxml2-utils ninja-build python3-jinja2 python3-yaml python3-setuptools shellcheck xsltproc


