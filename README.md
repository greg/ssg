# SCAP-Security-Guide builder

## What does this do?

[Builds the latest SCAP security guide content](https://complianceascode.readthedocs.io/en/latest/manual/developer/02_building_complianceascode.html) from [upstream sources](https://github.com/ComplianceAsCode/content) and saves the end results as [job artifacts](https://gitlab.com/greg/ssg/-/jobs/3582175118/artifacts/browse/content/build/).
